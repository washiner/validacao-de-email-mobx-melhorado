import 'package:mobx/mobx.dart';
part 'controller.g.dart';

class Controller = ControllerBase with _$Controller;

// a utilizacao do mixin Store e para geracao de codigos automaticos

abstract class ControllerBase with Store{

  ControllerBase(){
    //executa sempre que o observavel tem seu estado alterado
    autorun((_){
      print(email);
      print(senha);
      //print(emailSenha);
      print(formularioValidado);
    });
  }

  @observable
  String email = "";

  @observable
  String senha = "";

  @computed
  String get emailSenha => "$email - $senha";

  @computed
  bool get formularioValidado => email.length >= 5 && senha.length >= 5;


  @action
  void setEmail(valor)=> email = valor;
  @action
  void setSenha(valor)=> senha = valor;










  // @observable
  // int contador = 0;
  //
  // @action
  // incrementar(){
  //   contador++;
  // }


  //var _contador = Observable(0);
   // late Action incrementar;
   //
   // Controller(){
   //   incrementar = Action(_incrementar);
   // }
   //
   // int get contador => _contador.value;
   // set contador(int novoValor) => _contador.value = novoValor;
   //
   // _incrementar(){
   //   //contador.value = contador.value +1;
   //   contador++;
   // }
}