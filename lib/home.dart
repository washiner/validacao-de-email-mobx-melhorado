import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx_exemplo_jmt/controller.dart';
import 'package:mobx_exemplo_jmt/logado.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  // int _contador = 0;
  //
  // _incrementar(){
  //   setState(() {
  //     _contador++;
  //   });
  // }

  Controller controller = Controller();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: EdgeInsets.all(16),
        child: Column(
          children: [
            // Padding(
            //     padding: EdgeInsets.all(16),
            //   child: Text("${controller.contador}",style: TextStyle(fontSize: 60),),
            // ),
            Padding(
                padding: EdgeInsets.all(16),
              child: TextField(
                decoration: InputDecoration(labelText: "Email"),
                onChanged: controller.setEmail,
              ),

            ),
            Padding(
              padding: EdgeInsets.all(16),
              child: TextField(
                decoration: InputDecoration(labelText: "Senha"),
                onChanged: controller.setSenha,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16),
              child: Observer(
                builder: (_){
                  return Text(controller.formularioValidado ?"* Validado com Sucesso" : "ERROR nao validado"
                  );
                },
              )
            ),
            Padding(
                padding: EdgeInsets.all(16),
              child: Observer(
                  builder: (_){
                    return ElevatedButton(
                      child:Text("Logar", style: TextStyle(color: Colors.black, fontSize: 30),
                      ),
                      onPressed: controller.formularioValidado ? () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=> Logado()));
                      } : null,
                    );
                  }
              )

            )
          ],
        ),
        ),
    );
  }
}
