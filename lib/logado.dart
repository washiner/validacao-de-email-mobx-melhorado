import 'package:flutter/material.dart';

class Logado extends StatefulWidget {
  const Logado({Key? key}) : super(key: key);

  @override
  _LogadoState createState() => _LogadoState();
}

class _LogadoState extends State<Logado> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("LOGADO"),
      ),
    );
  }
}
